# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    return "S" * n + "0"

def S(n: str) -> str:
   return "S" + n 

def addition(a: str, b: str) -> str:
    na = len(a) - 1
    nb = len(b) - 1
    return nombre_entier(na + nb)


def multiplication(a: str, b: str) -> str:
    na = len(a) - 1
    nb = len(b) - 1
    return nombre_entier(na * nb)


def facto_ite(n: int) -> int:
    fact = 1
    for i in range(n):
        fact += fact * i
    return fact
    pass

def facto_rec(n: int) -> int:
    if n == 0:
        return 1

    return n * facto_rec(n - 1) 

def fibo_rec(n: int) -> int:
    if n <= 1:
        return n

    return fibo_rec(n - 1) + fibo_rec(n - 2)

def fibo_ite(n: int) -> int:
    fib = [0, 1]
    for i in range(n - 1): 
        fib.append(fib[-1] + fib[-2])
    
    if n <= 1:
        return fib[n]

    return fib[-1]

def golden_phi(n: int) -> int:
    pass


def sqrt5(n: int) -> int:
    pass


def pow(a: float, n: int) -> float:
   return a ** n
